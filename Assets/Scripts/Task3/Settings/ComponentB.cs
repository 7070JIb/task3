﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComponentB : MonoBehaviour, IColorComponentChangeble
{
    public TMP_Text valueText;

    public void Awake()
    {
        SettingsPanel.itemChanged += UpdateUI;
    }

    public void ChangeB(int d)
    {
        SettingsPanel.chosenItem.B = Random.Range(0, 255);
        UpdateUI(SettingsPanel.chosenItem);
    }

    public void UpdateUI(IСolorable item)
    {
        valueText.text = item.B.ToString();
    }
}
