﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class SettingsPanel : MonoBehaviour
{
    public static IСolorable chosenItem = null;

    public static event UnityAction<IСolorable> itemChanged;

    IEnumerable<ColorfulItemBase> items;

    void Start()
    {
        items = FindObjectsOfType<ColorfulItemBase>();
        foreach (IСolorable item in items)
        {
            item.Initialize();
            item.R = Random.Range(0, 255);
            item.G = Random.Range(0, 255);
            item.B = Random.Range(0, 255);
        }
        SetItem(items.FirstOrDefault());
    }

    public static void SetItem(IСolorable сolorableItem)
    {
        chosenItem = сolorableItem;

        itemChanged?.Invoke(сolorableItem);
    }
}
