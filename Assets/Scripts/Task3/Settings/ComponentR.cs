﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComponentR : MonoBehaviour, IColorComponentChangeble
{
    public TMP_Text valueText;

    public void Awake()
    {
        SettingsPanel.itemChanged += UpdateUI;
    }

    public void ChangeR(int d)
    {
        SettingsPanel.chosenItem.R = Mathf.Clamp(SettingsPanel.chosenItem.R + d, 0, 255);
        UpdateUI(SettingsPanel.chosenItem);
    }

    public void UpdateUI(IСolorable item)
    {
        valueText.text = item.R.ToString();
    }
}
