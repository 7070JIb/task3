﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComponentG : MonoBehaviour, IColorComponentChangeble
{
    public TMP_Text valueText;
    public Slider slider;
    
    public void Awake()
    {
        SettingsPanel.itemChanged += UpdateUI;
    }

    public void ChangeG()
    {
        SettingsPanel.chosenItem.G = (int)slider.value;
        UpdateUI(SettingsPanel.chosenItem);
    }

    public void UpdateUI(IСolorable item)
    {
        valueText.text = item.G.ToString();
        slider.value = item.G;
    }

}
