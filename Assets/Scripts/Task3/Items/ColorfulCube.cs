﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ColorfulCube : ColorfulItemBase
{
    MeshRenderer mRenderer;

    public override void Initialize()
    {
        mRenderer = this.GetComponent<MeshRenderer>();
        color = mRenderer.material.color;
    }

    public override void ColorizeItem()
    {
        mRenderer.material.color = this.color;
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit)
           && hit.collider.TryGetComponent<ColorfulCube>(out ColorfulCube cube)
           && cube == this)
            {
                ChooseItem();
            }
        }
        else
#endif
        {
#if UNITY_ANDROID || UNITY_IPHONE
            if (Input.touchCount > 0)
            {
                foreach (var touch in Input.touches)
                {
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(touch.position), out RaycastHit hit)
                    && hit.collider.TryGetComponent<ColorfulCube>(out ColorfulCube cube)
                    && cube == this)
                    {
                        ChooseItem();
                    }
                }
            }
#endif

        }
    }
}


