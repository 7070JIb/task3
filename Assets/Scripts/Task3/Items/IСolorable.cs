﻿using UnityEngine;


public interface IСolorable
{
    void Initialize();

    int R { get; set; }
    int G { get; set; }
    int B { get; set; }
}

