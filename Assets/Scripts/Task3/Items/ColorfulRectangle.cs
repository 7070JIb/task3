﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

public class ColorfulRectangle : ColorfulItemBase
{
    private Image image;

    public override void Initialize()
    {
        image = this.GetComponent<Image>();
        color = image.color;
    }
    public override void ColorizeItem()
    {
        image.color = this.color;
    }

}

