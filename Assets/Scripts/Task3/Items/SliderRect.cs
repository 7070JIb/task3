﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderRect : MonoBehaviour
{
    private float sizeX;
    private float sizeY;

    private RectTransform rect;

    private float startHoldDx;
    private Vector2 referenceResolution;
    private float scalerWidthRatio;

    void Start()
    {
        rect = this.GetComponent<RectTransform>();
        referenceResolution = FindObjectOfType<CanvasScaler>().referenceResolution;

        sizeX = rect.anchorMax.x - rect.anchorMin.x;
        sizeY = rect.anchorMax.y - rect.anchorMin.y;
    }

    public void OnBeginDrag(BaseEventData data)
    {
        PointerEventData pData = data as PointerEventData;
        scalerWidthRatio = Screen.width * referenceResolution.x / (referenceResolution.y / 2f);
        startHoldDx = (pData.position.x - rect.position.x) / scalerWidthRatio;
    }


    public void OnDrag(BaseEventData data)
    {
        PointerEventData pData = data as PointerEventData;
        float anchorDx = (pData.position.x - rect.position.x) / scalerWidthRatio;
        rect.anchorMin = new Vector2(
            Mathf.Clamp(rect.anchorMin.x + anchorDx - startHoldDx, 0.1f, 1f - sizeX),
            0);
        rect.anchorMax = rect.anchorMin + new Vector2(sizeX, sizeY);
    }

}
