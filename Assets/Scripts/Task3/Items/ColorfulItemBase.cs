﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ColorfulItemBase : MonoBehaviour, IСolorable
{
    protected Color color;

    public int R { get => (int)(color.r * 255); set => SetColor(value, G, B); }
    public int G { get => (int)(color.g * 255); set => SetColor(R, value, B); }
    public int B { get => (int)(color.b * 255); set => SetColor(R, G, value); }

    protected void SetColor(int r, int g, int b)
    {
        color = new Color(Clamp(r), Clamp(g), Clamp(b));
        ColorizeItem();
    }

    float Clamp(int x) => Mathf.Clamp((float)x / 255, 0, 1);

    public abstract void ColorizeItem();

    public virtual void ChooseItem() => SettingsPanel.SetItem(this);


    public abstract void Initialize();
}
